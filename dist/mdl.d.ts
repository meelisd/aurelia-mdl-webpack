import 'material-design-lite/material';
export declare class MDLCustomAttribute {
    private element;
    private eventAggregator;
    value: any;
    constructor(element: any, eventAggregator: any);
    attached(): void;
}
