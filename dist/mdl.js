"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var aurelia_framework_1 = require('aurelia-framework');
require('material-design-lite/material');
var aurelia_event_aggregator_1 = require('aurelia-event-aggregator');
var mdlTypes = {
    badge: {
        js: [],
        html: ['mdl-badge'],
        fct: []
    },
    button: {
        html: ['mdl-button', 'mdl-js-button'],
        js: ['MaterialButton'],
        fct: [manageRipple]
    },
    card: {
        js: [],
        html: ['mdl-card'],
        fct: []
    },
    checkbox: {
        js: ['MaterialCheckbox'],
        html: ['mdl-checkbox', 'mdl-js-checkbox'],
        fct: [manageRipple]
    },
    chip: {
        js: [],
        html: ['mdl-chip'],
        fct: []
    },
    'data-table': {
        js: ['MaterialDataTable'],
        html: ['mdl-data-table', 'mdl-js-data-table'],
        fct: [manageRipple]
    },
    dialog: {
        js: [],
        html: ['mdl-dialog'],
        fct: []
    },
    'mega-footer': {
        js: [],
        html: ['mdl-mega-footer'],
        fct: []
    },
    'mini-footer': {
        js: [],
        html: ['mdl-mini-footer'],
        fct: []
    },
    grid: {
        js: [],
        html: ['mdl-grid'],
        fct: []
    },
    'icon-toggle': {
        js: ['MaterialIconToggle'],
        html: ['mdl-icon-toggle', 'mdl-js-icon-toggle'],
        fct: [manageRipple]
    },
    layout: {
        js: ['MaterialLayout'],
        html: ['mdl-layout', 'mdl-js-layout'],
        fct: [manageRipple]
    },
    list: {
        js: [],
        html: ['mdl-list'],
        fct: []
    },
    menu: {
        js: ['MaterialMenu'],
        html: ['mdl-menu', 'mdl-js-menu'],
        fct: [manageRipple]
    },
    progress: {
        js: ['MaterialProgress'],
        html: ['mdl-progress', 'mdl-js-progress'],
        fct: []
    },
    radio: {
        js: ['MaterialRadio'],
        html: ['mdl-radio', 'mdl-js-radio'],
        fct: [manageRipple]
    },
    slider: {
        js: ['MaterialSlider'],
        html: ['mdl-slider', 'mdl-js-slider'],
        fct: []
    },
    snackbar: {
        js: ['MaterialSnackbar'],
        html: ['mdl-snackbar'],
        fct: []
    },
    spinner: {
        js: ['MaterialSpinner'],
        html: ['mdl-spinner', 'mdl-js-spinner'],
        fct: []
    },
    switch: {
        js: ['MaterialSwitch'],
        html: ['mdl-switch', 'mdl-js-switch'],
        fct: [manageRipple]
    },
    tabs: {
        js: ['MaterialTabs'],
        html: ['mdl-tabs', 'mdl-js-tabs'],
        fct: [manageRipple]
    },
    textfield: {
        js: ['MaterialTextfield'],
        html: ['mdl-textfield', 'mdl-js-textfield'],
        fct: []
    },
    tooltip: {
        js: ['MaterialTooltip'],
        html: ['mdl-tooltip'],
        fct: []
    },
    // Third party non official
    selectfield: {
        js: ['MaterialSelectfield'],
        html: ['mdl-selectfield'],
        fct: []
    }
};
function manageRipple(element) {
    if (element.classList.contains('mdl-js-ripple-effect')) {
        componentHandler.upgradeElement(element, 'MaterialRipple');
    }
    /**
     * Causes issues: upgrades nested elements that has mdl-js-ripple-effect class before the nested elements upgraded, marking them as
     * MaterialRipple upgraded, when the nested elements upgraded, and ripple upgrade tries to take place, it doesn't because it is already
     * marked as upgraded.
     * eventually causing rippleElement to be null on MaterialRipple.
     * NOTE: "mdl-js-ripple-effect" should be only on upgradable material elements, not on non material nested elements.
    let elements = element.querySelectorAll('.mdl-js-ripple-effect');
    for (let el of elements) {
      componentHandler.upgradeElement(el, 'MaterialRipple');
    }*/
    /** Some of the elements do require upgrade of nested elements, to avoid issues we must handle it carefully
     * NOTE: not sure about all the elements that require nested upgrading. Will add all the required when used and tested.
     */
    if (element.MaterialIconToggle || element.MaterialCheckbox) {
        /* We need to upgrade immediate children only, no easy way to do it (for all browsers) */
        var children = element.children;
        if (children) {
            for (var i = 0; i < children.length; i++) {
                var child = children[i];
                if (child.classList.contains('mdl-js-ripple-effect')) {
                    componentHandler.upgradeElement(child, 'MaterialRipple');
                }
            }
        }
    }
}
function upgradeElement(element, type) {
    var _a = (mdlTypes[type] || {}), _b = _a.html, html = _b === void 0 ? null : _b, _c = _a.fct, fct = _c === void 0 ? null : _c, _d = _a.js, js = _d === void 0 ? null : _d;
    if (html) {
        for (var _i = 0, html_1 = html; _i < html_1.length; _i++) {
            var h = html_1[_i];
            element.classList.add(h);
        }
    }
    for (var _e = 0, js_1 = js; _e < js_1.length; _e++) {
        var t = js_1[_e];
        componentHandler.upgradeElement(element, t);
    }
    for (var _f = 0, fct_1 = fct; _f < fct_1.length; _f++) {
        var f = fct_1[_f];
        f(element);
    }
}
var MDLCustomAttribute = (function () {
    function MDLCustomAttribute(element, eventAggregator) {
        this.element = element;
        this.eventAggregator = eventAggregator;
        this.element = element;
        this.eventAggregator = eventAggregator;
    }
    MDLCustomAttribute.prototype.attached = function () {
        //console.log("mdl attached: " + this.value);
        upgradeElement(this.element, this.value);
        var payload = { publisher: this, data: this.element };
        this.eventAggregator.publish('mdl:component:upgrade', payload);
    };
    MDLCustomAttribute = __decorate([
        aurelia_framework_1.inject(Element, aurelia_event_aggregator_1.EventAggregator),
        aurelia_framework_1.customAttribute('mdl'), 
        __metadata('design:paramtypes', [Object, Object])
    ], MDLCustomAttribute);
    return MDLCustomAttribute;
}());
exports.MDLCustomAttribute = MDLCustomAttribute;
//# sourceMappingURL=mdl.js.map