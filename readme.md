# aurelia-mdl-webpack

Typescript implementation of the [aurelia-mdl plugin](https://github.com/genadis/aurelia-mdl) by Genadi Sokolov, modified to play well with Webpack.

## Installation
1. Install the plugin:
    ```
    npm install aurelia-mdl-webpack --save
    ```
2. Make sure you use [manual bootstrapping](http://aurelia.io/hub.html#/doc/article/aurelia/framework/latest/app-configuration-and-startup/4)
3. Update your bootstrapping:
    ```typescript
    import 'material-design-lite/material';
    import 'material-design-lite/material.css';

    export async function configure(aurelia: Aurelia) {
    aurelia.use
        .standardConfiguration()
        .developmentLogging()
        .plugin("aurelia-mdl-webpack")

    await aurelia.start()
        .then(()=>aurelia.setRoot('app'))
    }
    ```
## Usage
From Genadi's [readme](https://github.com/genadis/aurelia-mdl/blob/master/README.md): 
### Principles

We created the `CustomAttribute` `mdl` in charge of the registration of dynamic elements.

So instead of writing:

```html
<button class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored">
  <i class="material-icons">add</i>
</button>
```

You will write in your views:

```html
<button mdl="button" class="mdl-button--fab mdl-button--colored">
  <i class="material-icons">add</i>
</button>
```

`mdl` values are :

```json
[ 'badge',
  'button',
  'card',
  'checkbox',
  'chip',
  'data-table',
  'dialog',
  'grid',
  'icon-toggle',
  'layout',
  'list',
  'menu',
  'mega-footer',
  'mini-footer',
  'progress',
  'radio',
  'slider',
  'snackbar',
  'spinner',
  'switch',
  'tabs',
  'textfield',
  'tooltip' ]
```